#! /usr/bin/env python3
from db import *
from flask import Flask, render_template
app = Flask(__name__)
app.debug = True
global users
users = {}
@app.route('/')
def home():
    return render_template(
        "home.html")

@app.route('/user/')
@app.route('/user/<name>')
def user(name=None):
    db = DB()
    res = db.get(name)
    todos=[]
    for t in res:
        todos.append(t[1])
    return render_template(
        "user.html",
        name=name,
        todo=todos)

@app.route('/users')
def users_list():
    db = DB()
    dico={}
    data=[]
    list_users = db.users()
    for us in list_users:
        if us[0] not in dico:
            dico[us[0]]=[]
        dico[us[0]].append(us[1])
    for key in dico:
        data.append((key, dico[key]))
    print(data)
    return render_template(
        "users.html",
        data=data)


if __name__ == '__main__':
    app.run(host="0.0.0.0")
